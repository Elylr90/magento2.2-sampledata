<?php
/**
 * Created by PhpStorm.
 * User: TrongPhung
 * Date: 5/12/17
 * Time: 11:35 AM
 */

namespace Magenest\Countdown\Block\Product;

use Magento\Framework\View\Element\Template;

/***
 * Class Clock
 * @package Magenest\Countdown\Block\Product
 */
class Clock extends \Magento\Catalog\Block\Product\ListProduct
{
    /***
     * @var \Magento\Catalog\Model\Product;
     */
    protected $product;
    /***
     * @var \Magenest\Countdown\Model\Countdown;
     */
    protected $countdown;

    /***
     * @param $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /***
     * @return int
     */
    public function getProductId()
    {
        return $this->product->getId();
    }

    /***
     * @return mixed
     */
    public function isCountdownEnabled()
    {
        return $this->product->getData('countdown_enabled');
    }

    /***
     * @return mixed
     */
    public function getFromDate()
    {
        return $this->countdown->getFromDate();
    }

    /***
     * @return mixed
     */
    public function getToDate()
    {
        return $this->countdown->getToDate();
    }

    /***
     * @return bool
     */
    public function getReadyCountDown()
    {
        if ($this->_scopeConfig->getValue('countdown/general/display_list') == 'enable') {
            $currentDate =  date('d-m-Y');
            $todate      =  $this->countdown->getToDate();
            $fromdate    =  $this->countdown->getFromDate();
            if (strtotime($todate) >= strtotime($currentDate) && strtotime($fromdate) <= strtotime($currentDate)) {
                return true;
            }
        }
        return false;
    }

    /***
     * @return bool
     */
    public function getRuleId()
    {
        $collection = \Magento\Framework\App\ObjectManager::getInstance()
            ->create('\Magenest\Countdown\Model\CatalogRule')->getCollection();
        foreach ($collection as $data) {
            if ($data->getProductId() == $this->getProductId()) {
                return $data->getRuleId();
            }
        }
        return false;
    }

    /***
     * @return bool
     */
    public function getCountdown()
    {
        if ($this->getRuleId() != false) {
            $collection = \Magento\Framework\App\ObjectManager::getInstance()
                ->create('\Magenest\Countdown\Model\Countdown')->getCollection();
            foreach ($collection as $data) {
                if ($data->getRuleId() == $this->getRuleId()) {
                    $this->countdown = $data;
                    return true;
                }
            }
        }
        return false;
    }
}
