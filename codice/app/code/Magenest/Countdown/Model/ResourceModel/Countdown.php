<?php

namespace Magenest\Countdown\Model\ResourceModel;

/***
 * Class Countdown
 * @package Magenest\Countdown\Model\ResourceModel
 */
class Countdown extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('magenest_countdown', 'countdown_id');
    }
}
