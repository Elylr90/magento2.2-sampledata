<?php
/**
 * Created by PhpStorm.
 * User: TrongPhung
 * Date: 4/19/17
 * Time: 5:11 PM
 */
namespace Magenest\Countdown\Observer\Campaign;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/***
 * Class Update
 * @package Magenest\Countdown\Observer\Campaign
 */
class Update implements ObserverInterface
{
    /**
     * Core Config Data
     *
     * @var $_scopeConfig \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magenest\Countdown\Model\CountdownFactory
     */
    protected $_countdownFactory;
    /**
     * Update constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magenest\Countdown\Model\CountdownFactory $countdownFactory,
        \Psr\Log\LoggerInterface $logger,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_countdownFactory = $countdownFactory;
        $this->_logger = $logger;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param $ruleId
     * @return bool
     */
    public function hasSetup($ruleId)
    {
        $collection = \Magento\Framework\App\ObjectManager::getInstance()
            ->create('\Magenest\Countdown\Model\Countdown')->getCollection();
        foreach ($collection as $data) {
            if ($data->getRuleId() == $ruleId) {
                return true;
            }
        }
        return false;
    }
    /**
     * Admin edit Catalog Rule Price
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $rule = $observer->getEvent()->getRule();
        if ($this->hasSetup($rule->getId())) {
            $collection = $this->_countdownFactory->create()->load($rule->getId(), 'rule_id');
            $collection->setNameRule($rule->getName());
            $collection->setFromDate($rule->getFromDate());
            $collection->setToDate($rule->getToDate());
            $collection->save();
            return;
        } else {
            if ($this->_scopeConfig->getValue('countdown/general/use_default')) {
                $collection = $this->_countdownFactory->create();
                $collection->setRuleId($rule->getId());
                $collection->setNameRule($rule->getName());
                $collection->setFromDate($rule->getFromDate());
                $collection->setToDate($rule->getToDate());
                $collection->setTitleClock($this->_scopeConfig->getValue('countdown/default/title_clock'));
                $collection->setTypeClock($this->_scopeConfig->getValue('countdown/default/type_clock'));
                $collection->setSizeClock($this->_scopeConfig->getValue('countdown/default/size_clock'));
                $collection->setColorClock($this->_scopeConfig->getValue('countdown/default/color_clock'));
                $collection->setTextColor($this->_scopeConfig->getValue('countdown/default/text_color'));
                $collection->save();
            } else {
                $collection = $this->_countdownFactory->create();
                $collection->setRuleId($rule->getId());
                $collection->setNameRule($rule->getName());
                $collection->setFromDate($rule->getFromDate());
                $collection->setToDate($rule->getToDate());
                $collection->setTitleClock($rule->getName());
                $collection->save();
            }
            return;
        }
    }
}
