<?php
/**
 * Created by PhpStorm.
 * User: TrongPhung
 * Date: 4/21/17
 * Time: 11:27 PM
 */
namespace Magenest\Countdown\Block\Adminhtml\Clock\Edit;

use Magenest\Countdown\Model\Config\ShowInList;

/**
 * Class Form
 * @package Magenest\Countdown\Block\Adminhtml\Clock\Edit
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magenest\Countdown\Model\Config\TypeClock
     */
    protected $_typeClock;
    /**
     * @var \Magenest\Countdown\Model\Config\SizeClock
     */
    protected $_sizeClock;
    /***
     * @var ShowInList
     */
    protected $_displayList;
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Customer\Api\GroupRepositoryInterface
     */
    protected $_groupRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Convert\DataObject
     */
    protected $_objectConverter;

    /***
     * Form constructor.
     * @param \Magenest\Countdown\Model\Config\TypeClock $typeClock
     * @param \Magenest\Countdown\Model\Config\SizeClock $sizeClock
     * @param ShowInList $displayList
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Customer\Api\GroupRepositoryInterface $groupRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Convert\DataObject $objectConverter
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magenest\Countdown\Model\Config\TypeClock $typeClock,
        \Magenest\Countdown\Model\Config\SizeClock $sizeClock,
        \Magenest\Countdown\Model\Config\ShowInList $displayList,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Convert\DataObject $objectConverter,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_groupRepository = $groupRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_objectConverter = $objectConverter;
        $this->_typeClock = $typeClock;
        $this->_sizeClock = $sizeClock;
        $this->_displayList = $displayList;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    
    protected function _construct()
    {
        parent::_construct();
        $this->setId('countdown_id');
        $this->setTitle(__('Clock Information'));
    }


    /**
     * prepare form
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        /**
         *
         */
        $model = $this->_coreRegistry->registry('countdown');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );


        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Clock Information')]);

        if ($model->getId()) {
            $fieldset->addField(
                'countdown_id',
                'hidden',
                [
                    'name' =>'countdown_id'
                ]
            );
        }

        $fieldset->addField(
            'title_clock',
            'text',
            [
                'name' => 'title_clock',
                'label' => __('Title Clock'),
                'title' => __('Title Clock'),
                'required' => true,
                'style' => 'width:50%'
            ]
        );
        $fieldset->addField(
            'type_clock',
            'select',
            [
                'name' => 'type_clock',
                'label' => __('Type Clock'),
                'title' => __('Type Clock'),
                'required' => true,
                'options' => $this->_typeClock->toArray(),
                'style' => 'width:50%'
            ]
        );
        $fieldset->addField(
            'size_clock',
            'select',
            [
                'name' => 'size_clock',
                'label' => __('Size Clock'),
                'title' => __('Size Clock'),
                'required' => true,
                'options' => $this->_sizeClock->toArray(),
                'style' => 'width:50%'
            ]
        );
         $fieldset->addField(
             'color_clock',
             'text',
             [
                'name' => 'color_clock',
                'label' => __('Color Clock'),
                'title' => __('Color Clock'),
                'class' => 'jscolor {hash:true, refine:false, adjust: false}',
                'required' => true,
                'style' => 'width:50%'

             ]
         );
        $fieldset->addField(
            'text_color',
            'text',
            [
                'name' => 'text_color',
                'label' => __('Text Color'),
                'title' => __('Text Color'),
                'class' => 'jscolor {hash:true, refine:false, adjust: false}',
                'required' => true,
                'style' => 'width:50%'
            ]
        );
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('General');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('General');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
