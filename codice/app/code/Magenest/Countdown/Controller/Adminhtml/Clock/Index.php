<?php
/**
 * Created by PhpStorm.
 * User: TrongPhung
 * Date: 4/22/17
 * Time: 9:03 AM
 */
namespace Magenest\Countdown\Controller\Adminhtml\Clock;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/***
 * Class Index
 * @package Magenest\Countdown\Controller\Adminhtml\Clock
 */
class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
    
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /***
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Countdown::clock');
        $resultPage->addBreadcrumb(__('Countdown'), __('Countdown'));
        $resultPage->addBreadcrumb(__('Manage Countdown'), __('Manage Countdown'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Countdown'));
        return $resultPage;
    }

    /***
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Countdown::clock');
    }
}
