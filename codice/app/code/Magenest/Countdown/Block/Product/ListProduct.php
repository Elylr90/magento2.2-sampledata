<?php
/**
 * Created by PhpStorm.
 * User: TrongPhung
 * Date: 5/12/17
 * Time: 11:07 AM
 */
namespace Magenest\Countdown\Block\Product;

/***
 * Class ListProduct
 * @package Magenest\Countdown\Block\Product
 */
class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    /***
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProductDetailsHtml(\Magento\Catalog\Model\Product $product)
    {
        $blockProduct = $this->getLayout()->createBlock('Magenest\Countdown\Block\Product\Clock');
        $blockProduct->setProduct($product);
        $html = $blockProduct->setTemplate('Magenest_Countdown::cart_clock.phtml')->toHtml();
        $renderer = $this->getDetailsRenderer($product->getTypeId());
        if ($renderer) {
            $renderer->setProduct($product);
            return $html.$renderer->toHtml();
        }
        return '';
    }
}
