<?php
/**
 * Created by PhpStorm.
 * User: TrongPhung
 * Date: 4/22/17
 * Time: 3:05 PM
 */
namespace Magenest\Countdown\Model;

/***
 * Class CatalogRule
 * @package Magenest\Countdown\Model
 */
class CatalogRule extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('Magenest\Countdown\Model\ResourceModel\CatalogRule');
    }
}
