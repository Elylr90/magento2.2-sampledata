<?php

namespace Magenest\Countdown\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/***
 * Class CatalogRule
 * @package Magenest\Countdown\Model\ResourceModel
 */
class CatalogRule extends AbstractDb
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('catalogrule_product', 'rule_product_id');
    }
}
