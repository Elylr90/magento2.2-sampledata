<?php
/**
 * Created by PhpStorm.
 * User: TrongPhung
 * Date: 4/22/17
 * Time: 3:06 PM
 */
namespace Magenest\Countdown\Model\ResourceModel\CatalogRule;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/***
 * Class Collection
 * @package Magenest\Countdown\Model\ResourceModel\CatalogRule
 */
class Collection extends AbstractCollection
{


    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('Magenest\Countdown\Model\CatalogRule', 'Magenest\Countdown\Model\ResourceModel\CatalogRule');
    }
}
