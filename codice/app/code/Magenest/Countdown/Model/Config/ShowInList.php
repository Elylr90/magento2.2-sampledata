<?php
/**
 * Created by PhpStorm.
 * User: TrongPhung
 * Date: 5/13/17
 * Time: 10:27 AM
 */
namespace Magenest\Countdown\Model\Config;

/***
 * Class ShowInList
 * @package Magenest\Countdown\Model\Config
 */
class ShowInList implements \Magento\Framework\Option\ArrayInterface
{
    /***
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'enable',
                'label' => __('Enable'),
            ],
            [
                'value' => 'disable',
                'label' => __('Disable'),
            ],
        ];
        // TODO: Implement toOptionArray() method.
    }

    /***
     * @return array
     */
    public function toArray()
    {
        return [
            "enable" => "Enable",
            "disable" => "Disable",
        ];
    }
}
