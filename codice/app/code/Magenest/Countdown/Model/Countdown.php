<?php
namespace Magenest\Countdown\Model;

/***
 * Class Countdown
 * @package Magenest\Countdown\Model
 */
class Countdown extends \Magento\Framework\Model\AbstractModel
{
    /**
     *
     */
    public function _construct()
    {
        $this-> _init('Magenest\Countdown\Model\ResourceModel\Countdown');
    }
}
