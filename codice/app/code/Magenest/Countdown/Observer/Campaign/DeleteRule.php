<?php
/**
 * Created by PhpStorm.
 * User: TrongPhung
 * Date: 4/19/17
 * Time: 5:11 PM
 */
namespace Magenest\Countdown\Observer\Campaign;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/***
 * Class DeleteRule
 * @package Magenest\Countdown\Observer\Campaign
 */
class DeleteRule implements ObserverInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magenest\Countdown\Model\CountdownFactory
     */
    protected $_countdownFactory;
    /**
     * Update constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magenest\Countdown\Model\CountdownFactory $countdownFactory,
        \Psr\Log\LoggerInterface $logger,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_countdownFactory = $countdownFactory;
        $this->_logger = $logger;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Admin edit Catalog Rule Price
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $rule = $observer->getEvent()->getRule();
        $collection = $this->_countdownFactory->create();
        $collection->load($rule->getId(), 'rule_id')->delete();
        return;
    }
}
