<?php

namespace Magenest\Countdown\Model\Config;

/***
 * Class SizeClock
 * @package Magenest\Countdown\Model\Config
 */
class SizeClock implements \Magento\Framework\Option\ArrayInterface
{
    /***
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'small',
                'label' => __('Small'),
            ],
            [
                'value' => 'medium',
                'label' => __('Medium'),
            ],
            [
                'value' => 'big',
                'label' => __('Big'),
            ],
        ];
        // TODO: Implement toOptionArray() method.
    }

    /***
     * @return array
     */
    public function toArray()
    {
        return [
            "small" => "Small",
            "medium" => "Medium",
            "big" => "Big",
        ];
    }
}
