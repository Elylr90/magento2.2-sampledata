<?php
namespace Magenest\Countdown\Model\ResourceModel\Countdown;

/***
 * Class Collection
 * @package Magenest\Countdown\Model\ResourceModel\Countdown
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Magenest\Countdown\Model\Countdown', 'Magenest\Countdown\Model\ResourceModel\Countdown');
    }
}
