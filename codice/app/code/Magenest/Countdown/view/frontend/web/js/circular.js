/**
 * Created by TrongPhung on 5/3/17.
 */
function circular(countDownDate){
    var x = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        var secondscanvas = document.getElementById("secondsCanvas");
        var cseconds = secondscanvas.getContext("2d");
        cseconds.clearRect(0,0,size,size);
        cseconds.beginPath();
        cseconds.font = textsize+"px Arial";
        cseconds.fillStyle = textColor;
        if (seconds<10) {
            cseconds.fillText("0"+seconds,xtext,ytext);
        }
        else {
            cseconds.fillText(seconds,xtext,ytext);
        }
        cseconds.arc(xcircle,ycircle,wcircle,1.5*Math.PI,1.5*Math.PI+(seconds/60)*2*Math.PI);
        cseconds.strokeStyle = colorClock;
        cseconds.stroke();
        var hourscanvas = document.getElementById("hoursCanvas");
        var chours = hourscanvas.getContext("2d");
        chours.clearRect(0,0,size,size);
        chours.beginPath();
        chours.fillStyle = textColor;
        chours.font = textsize+"px Arial";
        if (hours<10) {
            chours.fillText("0"+hours,xtext,ytext);
        }
        else {
            chours.fillText(hours,xtext,ytext);
        }
        chours.arc(xcircle,ycircle,wcircle,1.5*Math.PI,1.5*Math.PI+(hours/24)*2*Math.PI);
        chours.strokeStyle = colorClock;
        chours.stroke();
        var minutescanvas = document.getElementById("minutesCanvas");
        var cminutes = minutescanvas.getContext("2d");
        cminutes.clearRect(0,0,size,size);
        cminutes.beginPath();
        cminutes.fillStyle = textColor;
        cminutes.font = textsize+"px Arial";
        if (minutes<10) {
            cminutes.fillText("0"+minutes,xtext,ytext);
        }
        else {
            cminutes.fillText(minutes,xtext,ytext);
        }
        cminutes.arc(xcircle,ycircle,wcircle,1.5*Math.PI,1.5*Math.PI+(minutes/60)*2*Math.PI);
        cminutes.strokeStyle = colorClock;
        cminutes.stroke();
        var dayscanvas = document.getElementById("daysCanvas");
        var cdays = dayscanvas.getContext("2d");
        cdays.clearRect(0,0,size,size);
        cdays.beginPath();
        cdays.fillStyle = textColor;
        cdays.font = textsize+"px Arial";
        if (days<10) {
            cdays.fillText("0"+days,xtext,ytext);
        }
        else {
            cdays.fillText(days,xtext,ytext);
        }
        cdays.arc(xcircle,ycircle,wcircle,1.5*Math.PI,1.5*Math.PI+(days/30)*2*Math.PI);
        cdays.strokeStyle = colorClock;
        cdays.stroke();
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("circular_clock").innerHTML = "<p>EXPIRED</p>";
        }
    }, 1000);
}