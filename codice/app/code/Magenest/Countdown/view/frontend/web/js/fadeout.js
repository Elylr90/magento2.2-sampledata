/**
 * Created by TrongPhung on 5/3/17.
 */
function fadeout(countDownDate) {
    var x = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24)) < 10 ? "0"+Math.floor(distance / (1000 * 60 * 60 * 24)): Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (60 * 60)) < 10 ? "0"+Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) : Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)) < 10 ? "0"+Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)) : Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000) < 10 ? "0"+Math.floor((distance % (1000 * 60)) / 1000) : Math.floor((distance % (1000 * 60)) / 1000);
        document.getElementById("seconds").innerHTML = "<div id = 'fadeseconds'>"+ seconds + "</div>";
        if (seconds == "59") {
            document.getElementById("minutes").innerHTML = "<div id = 'fade'>"+ minutes + "</div>";
        } else {
            document.getElementById("minutes").innerHTML = minutes;
        }
        if (minutes == "59" && seconds == "59") {
            document.getElementById("hours").innerHTML = "<div id = 'fade'>"+ hours + "</div>";
        } else {
            document.getElementById("hours").innerHTML = hours;
        }
        if (hours == "23" && minutes == "59" && seconds == "59") {
            document.getElementById("days").innerHTML = "<div id = 'fade'>"+ days + "</div>";
        } else {
            document.getElementById("days").innerHTML = days;
        }
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("fadeout_clock").innerHTML = "<p>EXPIRED</p>";
        }
    }, 1000);
}
