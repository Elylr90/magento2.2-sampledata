CHANGELOG - Countdown
=============

### Added
[1.0.0] - 2017-05-13
=============
*Display Clock Countdown for Sale's Campaign in Product's Details.
*Display Clock Countdown in Product's Cart.
*Customize Style of Clock such as color,text color,...
*Update clock to the campaigns which have set.