<?php
/**
 * Created by PhpStorm.
 * User: TrongPhung
 * Date: 4/22/17
 * Time: 9:07 AM
 */
namespace Magenest\Countdown\Controller\Adminhtml\Clock;

/***
 * Class Save
 * @package Magenest\Countdown\Controller\Adminhtml\Clock
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($post);
            $array = [
                'title_clock' =>$post['title_clock'],
                'type_clock' =>$post['type_clock'],
                'size_clock' =>$post['size_clock'],
                'color_clock' =>$post['color_clock'],
                'text_color' =>$post['text_color']
            ];
            $model = $this->_objectManager->create('Magenest\Countdown\Model\Countdown');
            if (isset($post['countdown_id'])) {
                $model->load($post['countdown_id']);
            }
            $model->addData($array);
            $model->setData($post);
            $model->save();
            $this->messageManager->addSuccess(__('The countdown has been saved.'));
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
            if ($this->getRequest()->getParam('back')) {
                return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
            }
            return $resultRedirect->setPath('*/*/');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError($e, __('Something went wrong while saving the rule.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($post);
            return $resultRedirect->setPath('countdown/clock/edit', ['id'=>$this->getRequest()->getParam('id')]);
        }
    }



    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
