<?php

namespace Magenest\Countdown\Model\Config;

/***
 * Class TypeClock
 * @package Magenest\Countdown\Model\Config
 */
class TypeClock implements \Magento\Framework\Option\ArrayInterface
{
    /***
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'draft',
                'label' => __('Draft Clock'),
            ],
            [
                'value' => 'circular',
                'label' => __('Circular Clock'),
            ],
            [
                'value' => 'simple',
                'label' => __('Simple Clock'),
            ],
            [
                'value' => 'fadeout',
                'label' => __('Fadeout Clock'),
            ],
        ];
        // TODO: Implement toOptionArray() method.
    }

    /***
     * @return array
     */
    public function toArray()
    {
        return [
            "draft" => "Draft Clock",
            "circular" => "Circular Clock",
            "simple" => "Simple Clock",
            "fadeout" => "Fadeout Clock",
        ];
    }
}
