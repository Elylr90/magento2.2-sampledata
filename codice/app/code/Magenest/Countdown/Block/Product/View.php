<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Countdown\Block\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;

/***
 * Class View
 * @package Magenest\Countdown\Block\Product
 */
class View extends \Magento\Catalog\Block\Product\View
{
    /**
     * @var
     */
    protected $countdown;

    /***
     * @return mixed
     */
    public function isCountdownEnabled()
    {
        return $this->getProduct()->getData('countdown_enabled');
    }

    /***
     * @return mixed
     */
    public function getTitle()
    {
        return $this->_scopeConfig->getValue('countdown/default/title_clock');
    }

    /***
     * @return mixed
     */
    public function getToDate()
    {
        return $this->countdown->getToDate();
    }

    /***
     * @return mixed
     */
    public function getFromDate()
    {
        return $this->countdown->getFromDate();
    }

    /***
     * @return bool
     */
    public function getReadyCountDown()
    {
        if ($this->_scopeConfig->getValue('countdown/general/show_clock_countdown')) {
            $currentDate =  date('d-m-Y');
            $todate      =  $this->countdown->getToDate();
            $fromdate    =  $this->countdown->getFromDate();
            if (strtotime($todate) >= strtotime($currentDate) && strtotime($fromdate) <= strtotime($currentDate)) {
                return true;
            }
        }
        return false;
    }

    /***
     * @return bool
     */
    public function getRuleId()
    {
        $collection = \Magento\Framework\App\ObjectManager::getInstance()
            ->create('\Magenest\Countdown\Model\CatalogRule')->getCollection();
        foreach ($collection as $data) {
            if ($data->getProductId() == $this->getProductId()) {
                return $data->getRuleId();
            }
        }
        return false;
    }

    /***
     * @return int
     */
    public function getProductId()
    {
        return $this->getProduct()->getId();
    }

    /***
     * @return bool
     */
    public function getCountdown()
    {
        if ($this->getRuleId() != false) {
            $collection = \Magento\Framework\App\ObjectManager::getInstance()
                ->create('\Magenest\Countdown\Model\Countdown')->getCollection();
            foreach ($collection as $data) {
                if ($data->getRuleId() == $this->getRuleId()) {
                    $this->countdown = $data;
                    return true;
                }
            }
        }
        return false;
    }

    /***
     * @return array|bool
     */
    public function prepareClockData()
    {
        if ($this->getCountdown()) {
            $clockData = [];
            $clockData['title_clock'] = $this->countdown->getTitleClock();
            $clockData['type_clock'] = $this->countdown->getTypeClock();
            $clockData['size_clock'] = $this->countdown->getSizeClock();
            $clockData['color_clock'] = $this->countdown->getColorClock();
            $clockData['text_color'] = $this->countdown->getTextColor();
            $clockData['countdown_id'] = $this->countdown->getCountdownId();
            if ($clockData['type_clock'] == "") {
                return null;
            }
            return $clockData;
        } else {
            return null;
        }
    }
}
